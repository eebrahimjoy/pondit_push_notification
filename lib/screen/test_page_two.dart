import 'package:flutter/material.dart';

class TestPageTwo extends StatefulWidget {
  final message;

  TestPageTwo({this.message});
  @override
  _TestPageTwoState createState() => _TestPageTwoState();
}

class _TestPageTwoState extends State<TestPageTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellowAccent,
      body: Padding(
        padding: EdgeInsets.all(18),
        child: Center(
          child: Text(
            widget.message != null ? widget.message : "no message",
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}
