import 'package:flutter/material.dart';

class TestPageOne extends StatefulWidget {
  final String message;

  TestPageOne({this.message});

  @override
  _TestPageOneState createState() => _TestPageOneState();
}

class _TestPageOneState extends State<TestPageOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Padding(
        padding: EdgeInsets.all(18),
        child: Center(
          child: Text(
            widget.message != null ? widget.message : "No Message",
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}
