import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:push/localnotification/local_notifications.dart';
import 'package:push/screen/test_page_one.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    LocalNotificationService.initialize(context);
    //App closed but click to redirect the page you want
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        final messageFromFirebase = message.data["message"];
        Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (context) {
          return TestPageOne(
            message: messageFromFirebase,
          );
        }), (route) => false);
      }
    });
    //foreground
    FirebaseMessaging.onMessage.listen((message) {
      if (message.notification != null) {
        print(message.notification.body);
        print(message.notification.title);
      }
      LocalNotificationService.display(message);
    });

    // background but click the notification
    //onMessageOpenedApp

    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      final messageFromFirebase = message.data["message"];

      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) {
        return TestPageOne(
          message: messageFromFirebase,
        );
      }), (route) => false);
      //print(messageFromFirebase);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(18),
        child: Center(
          child: Text(
            'Will receive notification soon',
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}
